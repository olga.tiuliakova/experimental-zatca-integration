package trang2301;

import com.google.common.primitives.Bytes;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.Getter;
import lombok.Setter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

public class CreateZatcaQrCodeClient {

    public static void main(String[] args) throws WriterException, IOException {
        final var invoiceAsQrCode = new InvoiceAsQrCode(
                "Bobs Records",
                "310122393500003",
                ZonedDateTime.parse("2022-04-25T15:30:00Z"),
                "1000.00",
                "150.00");
        final String contentAsBase64 = invoiceAsQrCode.getContentAsBase64();
        System.out.println(contentAsBase64);
        // =======
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = writer.encode(contentAsBase64, BarcodeFormat.QR_CODE, 512, 512);
        BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
        File outputfile = new File("image.jpg");
        ImageIO.write(bufferedImage, "jpg", outputfile);
    }

    private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);
    public static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }

    public static byte[] numberToSingletonByteArray(int length) {
        return new byte[] { ((byte) length) };
    }

    @Getter
    enum FieldDescription {

        SELLER_NAME(1),
        SELLER_VAT_REGISTRATION_NUMBER(2),
        /**
         * Time stamp of the invoice (date and time) in accordance with ISO 8601 (example 3 2022-02-21T12:13:57Z)
         */
        INVOICE_TIMESTAMP(3),
        /**
         * Electronic Invoice or Credit/Debit Note total (with VAT)
         */
        TOTAL(4),
        VAT_TOTAL(5);

        private final int tag;
        private final byte[] tagAsBytes;

        FieldDescription(int tag) {
            this.tag = tag;
            this.tagAsBytes = numberToSingletonByteArray(tag);
        }

    }

    @Getter
    public static final class Field {
        private final FieldDescription description;
        @Setter
        private String valueAsString;

        public Field(FieldDescription fieldDescription, String valueAsString) {
            this.description = fieldDescription;
            this.valueAsString = valueAsString;
        }

        public Field(FieldDescription fieldDescription) {
            this(fieldDescription, null);
        }

        public byte[] toTagLengthValueFormatAsBytes() {
            if (description.tag <= 5 && description.tag >= 1) {
                byte[] valueAsBytes = valueAsString.getBytes(StandardCharsets.UTF_8);
                byte[] lengthAsBytes = numberToSingletonByteArray(valueAsBytes.length);
                byte[] bytes = Bytes.concat(description.getTagAsBytes(), lengthAsBytes, valueAsBytes);
                System.out.printf(
                        "\nTag: %s\tLength: %s\tValue: %s\n",
                        description.tag, valueAsString.length(), valueAsString
                );
                System.out.printf(
                        "Tag: %s\tLength: %s\tValue: %s\n",
                        bytesToHex(description.getTagAsBytes()), bytesToHex(lengthAsBytes), bytesToHex(valueAsBytes)
                );
                System.out.println(bytesToHex(bytes));
                return bytes;
            }
            // TODO
            return null;
        }

    }

    @Getter
    public static final class InvoiceAsQrCode {

        private final Field sellerName;
        private final Field sellerVatRegistrationNumber;
        private final Field invoiceTimestamp;
        private final Field total;
        private final Field vatTotal;

        public InvoiceAsQrCode(String sellerName, String sellerVatRegistrationNumber, ZonedDateTime invoiceTimestamp, String total, String vatTotal) {
            // TODO Validate params
            this.sellerName = new Field(FieldDescription.SELLER_NAME, sellerName);
            this.sellerVatRegistrationNumber = new Field(FieldDescription.SELLER_VAT_REGISTRATION_NUMBER, sellerVatRegistrationNumber);
            this.invoiceTimestamp = new Field(FieldDescription.INVOICE_TIMESTAMP, invoiceTimestamp.format(DateTimeFormatter.ISO_DATE));
            this.total = new Field(FieldDescription.TOTAL, String.valueOf(total));
            this.vatTotal = new Field(FieldDescription.VAT_TOTAL, String.valueOf(vatTotal));
        }

        public String getContentAsBase64() {
            final byte[] sellerNameAsBytes = sellerName.toTagLengthValueFormatAsBytes();
            final byte[] sellerVatRegistrationNumberAsBytes = sellerVatRegistrationNumber.toTagLengthValueFormatAsBytes();
            final byte[] invoiceTimestampAsBytes = invoiceTimestamp.toTagLengthValueFormatAsBytes();
            final byte[] totalAsBytes = total.toTagLengthValueFormatAsBytes();
            final byte[] vatTotalAsBytes = vatTotal.toTagLengthValueFormatAsBytes();
            return Base64.getEncoder().encodeToString(Bytes.concat(
                    sellerNameAsBytes,
                    sellerVatRegistrationNumberAsBytes,
                    invoiceTimestampAsBytes,
                    totalAsBytes,
                    vatTotalAsBytes
            ));
        }

    }

}
