package trang2301;

import com.gazt.einvoicing.qr.generation.service.impl.QRCodeGeneratorServiceImpl;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.zatca.sdk.service.QrGenerationService;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ZatcaSdkQrCodeCreator {


    public static void main(String[] args) throws Exception {
        var qrGenerator = new QRCodeGeneratorServiceImpl();
        var key = new byte[] {48, 86, 48, 16, 6, 7, 42, -122, 72, -50, 61, 2, 1, 6, 5, 43, -127, 4, 0, 10, 3, 66, 0, 4, 97, -125, 12, -96, -26, -123, 96, 8,
                76, 59, -5, 45, 122, -117, 95, 103, 38, -81, -81, -86, 117, -43, 36, -91, -62, -62, -67, 107, 57, -84, 45, -114, -37, -43, -65, -123, 46, 26, -116, 2, -72, 65, -39, -38, -121, 41, -70, 49, -88, -93, 95, -66, 66, -125, 120, -8, 105, -86, 59, -94, -26, 23, 39, -47};
        var qr = qrGenerator.generateQrCode("Ahmed Mohamed AL Ahmady", "300075588700003", "2022-03-13T14:40:40Z",
                "1108.90", "144.9", "Ulmp7fL8mDoVypC9T3Dt+9JJV8ERvH0v9GYjOu56MrQ=", key,
                "MEYCIQCVSSr6+lPwx4jzdaJmRPhQuClWa7x62dnveg+w3MrnxAIhANly7+PwBiZuw1f349BbnmCw51VRCKmTxvebE3eAGcBj");

        writeQr(qr, "image_by_sdk.jpg");

        var fatoooraGenerator  = new QrGenerationService();
        var fatootaQr = fatoooraGenerator.generateQrCode("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<Invoice xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ccts=\"urn:un:unece:uncefact:documentation:2\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><ext:UBLExtensions>\n" +
                "    <ext:UBLExtension>\n" +
                "        <ext:ExtensionURI>urn:oasis:names:specification:ubl:dsig:enveloped:xades</ext:ExtensionURI>\n" +
                "        <ext:ExtensionContent>\n" +
                "            <!-- Please note that the signature values are sample values only -->\n" +
                "            <sig:UBLDocumentSignatures xmlns:sig=\"urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2\" xmlns:sac=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2\" xmlns:sbc=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2\">\n" +
                "                <sac:SignatureInformation>\n" +
                "                    <cbc:ID>urn:oasis:names:specification:ubl:signature:1</cbc:ID>\n" +
                "                    <sbc:ReferencedSignatureID>urn:oasis:names:specification:ubl:signature:Invoice</sbc:ReferencedSignatureID>\n" +
                "                    <ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" Id=\"signature\">\n" +
                "                        <ds:SignedInfo>\n" +
                "                            <ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/2006/12/xml-c14n11\"/>\n" +
                "                            <ds:SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#ecdsa-sha256\"/>\n" +
                "                            <ds:Reference Id=\"invoiceSignedData\" URI=\"\">\n" +
                "                                <ds:Transforms>\n" +
                "                                    <ds:Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\">\n" +
                "                                        <ds:XPath>not(//ancestor-or-self::ext:UBLExtensions)</ds:XPath>\n" +
                "                                    </ds:Transform>\n" +
                "                                    <ds:Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\">\n" +
                "                                        <ds:XPath>not(//ancestor-or-self::cac:Signature)</ds:XPath>\n" +
                "                                    </ds:Transform>\n" +
                "                                    <ds:Transform Algorithm=\"http://www.w3.org/TR/1999/REC-xpath-19991116\">\n" +
                "                                        <ds:XPath>not(//ancestor-or-self::cac:AdditionalDocumentReference[cbc:ID='QR'])</ds:XPath>\n" +
                "                                    </ds:Transform>\n" +
                "                                    <ds:Transform Algorithm=\"http://www.w3.org/2006/12/xml-c14n11\"/>\n" +
                "                                </ds:Transforms>\n" +
                "                                <ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/>\n" +
                "                                <ds:DigestValue>Ulmp7fL8mDoVypC9T3Dt+9JJV8ERvH0v9GYjOu56MrQ=</ds:DigestValue>\n" +
                "                            </ds:Reference>\n" +
                "                            <ds:Reference Type=\"http://www.w3.org/2000/09/xmldsig#SignatureProperties\" URI=\"#xadesSignedProperties\">\n" +
                "                                <ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/>\n" +
                "                                <ds:DigestValue>OWE1NTY4ZTQyM2I0ZGZiZGU3MjQ4NzllZjRiNGZkZWYxYWRjZmE2MTgyY2JkMzRkMGE1NDMyNjQyYWNjNmUwNA==</ds:DigestValue>\n" +
                "                            </ds:Reference>\n" +
                "                        </ds:SignedInfo>\n" +
                "                        <ds:SignatureValue>MEYCIQDyatNxVH8g0idE5p5SwO0tjptUvruV6kn71Ju9J4hcDgIhALw90doEsY8wo+AX7ErBOUVnxIMcpEBzdDtWtXRt2XmE</ds:SignatureValue>\n" +
                "                        <ds:KeyInfo>\n" +
                "                            <ds:X509Data>\n" +
                "                                <ds:X509Certificate>MIID9jCCA5ugAwIBAgITbwAAeCy9aKcLA99HrAABAAB4LDAKBggqhkjOPQQDAjBjMRUwEwYKCZImiZPyLGQBGRYFbG9jYWwxEzARBgoJkiaJk/IsZAEZFgNnb3YxFzAVBgoJkiaJk/IsZAEZFgdleHRnYXp0MRwwGgYDVQQDExNUU1pFSU5WT0lDRS1TdWJDQS0xMB4XDTIyMDQxOTIwNDkwOVoXDTI0MDQxODIwNDkwOVowWTELMAkGA1UEBhMCU0ExEzARBgNVBAoTCjMxMjM0NTY3ODkxDDAKBgNVBAsTA1RTVDEnMCUGA1UEAxMeVFNULS05NzA1NjAwNDAtMzEyMzQ1Njc4OTAwMDAzMFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEYYMMoOaFYAhMO/steotfZyavr6p11SSlwsK9azmsLY7b1b+FLhqMArhB2dqHKboxqKNfvkKDePhpqjui5hcn0aOCAjkwggI1MIGaBgNVHREEgZIwgY+kgYwwgYkxOzA5BgNVBAQMMjEtVFNUfDItVFNUfDMtNDdmMTZjMjYtODA2Yi00ZTE1LWIyNjktN2E4MDM4ODRiZTljMR8wHQYKCZImiZPyLGQBAQwPMzEyMzQ1Njc4OTAwMDAzMQ0wCwYDVQQMDAQxMTAwMQwwCgYDVQQaDANUU1QxDDAKBgNVBA8MA1RTVDAdBgNVHQ4EFgQUO5ZiU7NakU3eejVa3I2S1B2sDwkwHwYDVR0jBBgwFoAUdmCM+wagrGdXNZ3PmqynK5k1tS8wTgYDVR0fBEcwRTBDoEGgP4Y9aHR0cDovL3RzdGNybC56YXRjYS5nb3Yuc2EvQ2VydEVucm9sbC9UU1pFSU5WT0lDRS1TdWJDQS0xLmNybDCBrQYIKwYBBQUHAQEEgaAwgZ0wbgYIKwYBBQUHMAGGYmh0dHA6Ly90c3RjcmwuemF0Y2EuZ292LnNhL0NlcnRFbnJvbGwvVFNaRWludm9pY2VTQ0ExLmV4dGdhenQuZ292LmxvY2FsX1RTWkVJTlZPSUNFLVN1YkNBLTEoMSkuY3J0MCsGCCsGAQUFBzABhh9odHRwOi8vdHN0Y3JsLnphdGNhLmdvdi5zYS9vY3NwMA4GA1UdDwEB/wQEAwIHgDAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwMwJwYJKwYBBAGCNxUKBBowGDAKBggrBgEFBQcDAjAKBggrBgEFBQcDAzAKBggqhkjOPQQDAgNJADBGAiEA7mHT6yg85jtQGWp3M7tPT7Jk2+zsvVHGs3bU5Z7YE68CIQD60ebQamYjYvdebnFjNfx4X4dop7LsEBFCNSsLY0IFaQ==</ds:X509Certificate>\n" +
                "                            </ds:X509Data>\n" +
                "                        </ds:KeyInfo>\n" +
                "                        <ds:Object>\n" +
                "                            <xades:QualifyingProperties xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" Target=\"signature\">\n" +
                "                                <xades:SignedProperties Id=\"xadesSignedProperties\">\n" +
                "                                    <xades:SignedSignatureProperties>\n" +
                "                                        <xades:SigningTime>2023-05-01T11:27:02Z</xades:SigningTime>\n" +
                "                                        <xades:SigningCertificate>\n" +
                "                                            <xades:Cert>\n" +
                "                                                <xades:CertDigest>\n" +
                "                                                    <ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/>\n" +
                "                                                    <ds:DigestValue>NjlhOTVmYzIzN2I0MjcxNGRjNDQ1N2EzM2I5NGNjNDUyZmQ5ZjExMDUwNGM2ODNjNDAxMTQ0ZDk1NDQ4OTRmYg==</ds:DigestValue>\n" +
                "                                                </xades:CertDigest>\n" +
                "                                                <xades:IssuerSerial>\n" +
                "                                                    <ds:X509IssuerName>CN=TSZEINVOICE-SubCA-1, DC=extgazt, DC=gov, DC=local</ds:X509IssuerName>\n" +
                "                                                    <ds:X509SerialNumber>2475382876776561391517206651645660279462721580</ds:X509SerialNumber>\n" +
                "                                                </xades:IssuerSerial>\n" +
                "                                            </xades:Cert>\n" +
                "                                        </xades:SigningCertificate>\n" +
                "                                    </xades:SignedSignatureProperties>\n" +
                "                                </xades:SignedProperties>\n" +
                "                            </xades:QualifyingProperties>\n" +
                "                        </ds:Object>\n" +
                "                    </ds:Signature>\n" +
                "                </sac:SignatureInformation>\n" +
                "            </sig:UBLDocumentSignatures>\n" +
                "        </ext:ExtensionContent>\n" +
                "    </ext:UBLExtension>\n" +
                "</ext:UBLExtensions>\n" +
                "    <cbc:ProfileID>reporting:1.0</cbc:ProfileID>\n" +
                "    <cbc:ID>SME00062</cbc:ID>\n" +
                "    <cbc:UUID>16e78469-64af-406d-9cfd-895e724198f0</cbc:UUID>\n" +
                "    <cbc:IssueDate>2022-03-13</cbc:IssueDate>\n" +
                "    <cbc:IssueTime>14:40:40</cbc:IssueTime>\n" +
                "    <cbc:InvoiceTypeCode name=\"0111010\">388</cbc:InvoiceTypeCode>\n" +
                "    <cbc:DocumentCurrencyCode>SAR</cbc:DocumentCurrencyCode>\n" +
                "    <cbc:TaxCurrencyCode>SAR</cbc:TaxCurrencyCode>\n" +
                "    <cac:AdditionalDocumentReference>\n" +
                "        <cbc:ID>ICV</cbc:ID>\n" +
                "        <cbc:UUID>62</cbc:UUID>\n" +
                "    </cac:AdditionalDocumentReference>\n" +
                "    <cac:AdditionalDocumentReference>\n" +
                "        <cbc:ID>PIH</cbc:ID>\n" +
                "        <cac:Attachment>\n" +
                "            <cbc:EmbeddedDocumentBinaryObject mimeCode=\"text/plain\">NWZlY2ViNjZmZmM4NmYzOGQ5NTI3ODZjNmQ2OTZjNzljMmRiYzIzOWRkNGU5MWI0NjcyOWQ3M2EyN2ZiNTdlOQ==</cbc:EmbeddedDocumentBinaryObject>\n" +
                "        </cac:Attachment>\n" +
                "    </cac:AdditionalDocumentReference>\n" +
                "    <cac:AdditionalDocumentReference>\n" +
                "        <cbc:ID>QR</cbc:ID>\n" +
                "        <cac:Attachment>\n" +
                "            <cbc:EmbeddedDocumentBinaryObject mimeCode=\"text/plain\">ARdBaG1lZCBNb2hhbWVkIEFMIEFobWFkeQIPMzAwMDc1NTg4NzAwMDAzAxQyMDIyLTAzLTEzVDE0OjQwOjQwWgQHMTEwOC45MAUFMTQ0LjkGLFVsbXA3Zkw4bURvVnlwQzlUM0R0KzlKSlY4RVJ2SDB2OUdZak91NTZNclE9B1gwVjAQBgcqhkjOPQIBBgUrgQQACgNCAARhgwyg5oVgCEw7+y16i19nJq+vqnXVJKXCwr1rOawtjtvVv4UuGowCuEHZ2ocpujGoo1++QoN4+GmqO6LmFyfRCCEA8mrTcVR/INInROaeUsDtLY6bVL67lepJ+9SbvSeIXA4JIQC8PdHaBLGPMKPgF+xKwTlFZ8SDHKRAc3Q7VrV0bdl5hA==</cbc:EmbeddedDocumentBinaryObject>\n" +
                "        </cac:Attachment>\n" +
                "</cac:AdditionalDocumentReference><cac:Signature>\n" +
                "      <cbc:ID>urn:oasis:names:specification:ubl:signature:Invoice</cbc:ID>\n" +
                "      <cbc:SignatureMethod>urn:oasis:names:specification:ubl:dsig:enveloped:xades</cbc:SignatureMethod>\n" +
                "</cac:Signature><cac:AccountingSupplierParty>\n" +
                "        <cac:Party>\n" +
                "            <cac:PartyIdentification>\n" +
                "                <cbc:ID schemeID=\"CRN\">454634645645654</cbc:ID>\n" +
                "            </cac:PartyIdentification>\n" +
                "            <cac:PostalAddress>\n" +
                "                <cbc:StreetName>test</cbc:StreetName>\n" +
                "                <cbc:BuildingNumber>3454</cbc:BuildingNumber>\n" +
                "                <cbc:PlotIdentification>1234</cbc:PlotIdentification>\n" +
                "                <cbc:CitySubdivisionName>test</cbc:CitySubdivisionName>\n" +
                "                <cbc:CityName>Riyadh</cbc:CityName>\n" +
                "                <cbc:PostalZone>12345</cbc:PostalZone>\n" +
                "                <cbc:CountrySubentity>test</cbc:CountrySubentity>\n" +
                "                <cac:Country>\n" +
                "                    <cbc:IdentificationCode>SA</cbc:IdentificationCode>\n" +
                "                </cac:Country>\n" +
                "            </cac:PostalAddress>\n" +
                "            <cac:PartyTaxScheme>\n" +
                "                <cbc:CompanyID>300075588700003</cbc:CompanyID>\n" +
                "                <cac:TaxScheme>\n" +
                "                    <cbc:ID>VAT</cbc:ID>\n" +
                "                </cac:TaxScheme>\n" +
                "            </cac:PartyTaxScheme>\n" +
                "            <cac:PartyLegalEntity>\n" +
                "                <cbc:RegistrationName>Ahmed Mohamed AL Ahmady</cbc:RegistrationName>\n" +
                "            </cac:PartyLegalEntity>\n" +
                "        </cac:Party>\n" +
                "    </cac:AccountingSupplierParty>\n" +
                "    <cac:AccountingCustomerParty>\n" +
                "        <cac:Party>\n" +
                "            <cac:PartyIdentification>\n" +
                "                <cbc:ID schemeID=\"NAT\">2345</cbc:ID>\n" +
                "            </cac:PartyIdentification>\n" +
                "            <cac:PostalAddress>\n" +
                "                <cbc:StreetName>baaoun</cbc:StreetName>\n" +
                "                <cbc:AdditionalStreetName>sdsd</cbc:AdditionalStreetName>\n" +
                "                <cbc:BuildingNumber>3353</cbc:BuildingNumber>\n" +
                "                <cbc:PlotIdentification>3434</cbc:PlotIdentification>\n" +
                "                <cbc:CitySubdivisionName>fgff</cbc:CitySubdivisionName>\n" +
                "                <cbc:CityName>Dhurma</cbc:CityName>\n" +
                "                <cbc:PostalZone>34534</cbc:PostalZone>\n" +
                "                <cbc:CountrySubentity>ulhk</cbc:CountrySubentity>\n" +
                "                <cac:Country>\n" +
                "                    <cbc:IdentificationCode>SA</cbc:IdentificationCode>\n" +
                "                </cac:Country>\n" +
                "            </cac:PostalAddress>\n" +
                "            <cac:PartyTaxScheme>\n" +
                "                <cac:TaxScheme>\n" +
                "                    <cbc:ID>VAT</cbc:ID>\n" +
                "                </cac:TaxScheme>\n" +
                "            </cac:PartyTaxScheme>\n" +
                "            <cac:PartyLegalEntity>\n" +
                "                <cbc:RegistrationName>sdsa</cbc:RegistrationName>\n" +
                "            </cac:PartyLegalEntity>\n" +
                "        </cac:Party>\n" +
                "    </cac:AccountingCustomerParty>\n" +
                "    <cac:Delivery>\n" +
                "        <cbc:ActualDeliveryDate>2022-03-13</cbc:ActualDeliveryDate>\n" +
                "        <cbc:LatestDeliveryDate>2022-03-15</cbc:LatestDeliveryDate>\n" +
                "    </cac:Delivery>\n" +
                "    <cac:PaymentMeans>\n" +
                "        <cbc:PaymentMeansCode>10</cbc:PaymentMeansCode>\n" +
                "    </cac:PaymentMeans>\n" +
                "    <cac:AllowanceCharge>\n" +
                "        <cbc:ID>1</cbc:ID>\n" +
                "        <cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "        <cbc:AllowanceChargeReason>discount</cbc:AllowanceChargeReason>\n" +
                "        <cbc:Amount currencyID=\"SAR\">2</cbc:Amount>\n" +
                "        <cac:TaxCategory>\n" +
                "            <cbc:ID schemeID=\"UN/ECE 5305\" schemeAgencyID=\"6\">S</cbc:ID>\n" +
                "            <cbc:Percent>15</cbc:Percent>\n" +
                "            <cac:TaxScheme>\n" +
                "                <cbc:ID schemeID=\"UN/ECE 5153\" schemeAgencyID=\"6\">VAT</cbc:ID>\n" +
                "            </cac:TaxScheme>\n" +
                "        </cac:TaxCategory>\n" +
                "    </cac:AllowanceCharge>\n" +
                "    <cac:TaxTotal>\n" +
                "        <cbc:TaxAmount currencyID=\"SAR\">144.9</cbc:TaxAmount>\n" +
                "        <cac:TaxSubtotal>\n" +
                "            <cbc:TaxableAmount currencyID=\"SAR\">966.00</cbc:TaxableAmount>\n" +
                "            <cbc:TaxAmount currencyID=\"SAR\">144.90</cbc:TaxAmount>\n" +
                "            <cac:TaxCategory>\n" +
                "                <cbc:ID schemeID=\"UN/ECE 5305\" schemeAgencyID=\"6\">S</cbc:ID>\n" +
                "                <cbc:Percent>15.00</cbc:Percent>\n" +
                "                <cac:TaxScheme>\n" +
                "                    <cbc:ID schemeID=\"UN/ECE 5153\" schemeAgencyID=\"6\">VAT</cbc:ID>\n" +
                "                </cac:TaxScheme>\n" +
                "            </cac:TaxCategory>\n" +
                "        </cac:TaxSubtotal>\n" +
                "    </cac:TaxTotal>\n" +
                "    <cac:TaxTotal>\n" +
                "        <cbc:TaxAmount currencyID=\"SAR\">144.9</cbc:TaxAmount>\n" +
                "    </cac:TaxTotal>\n" +
                "    <cac:LegalMonetaryTotal>\n" +
                "        <cbc:LineExtensionAmount currencyID=\"SAR\">966.00</cbc:LineExtensionAmount>\n" +
                "        <cbc:TaxExclusiveAmount currencyID=\"SAR\">964.00</cbc:TaxExclusiveAmount>\n" +
                "        <cbc:TaxInclusiveAmount currencyID=\"SAR\">1108.90</cbc:TaxInclusiveAmount>\n" +
                "        <cbc:AllowanceTotalAmount currencyID=\"SAR\">2.00</cbc:AllowanceTotalAmount>\n" +
                "        <cbc:PrepaidAmount currencyID=\"SAR\">0.00</cbc:PrepaidAmount>\n" +
                "        <cbc:PayableAmount currencyID=\"SAR\">1108.90</cbc:PayableAmount>\n" +
                "    </cac:LegalMonetaryTotal>\n" +
                "    <cac:InvoiceLine>\n" +
                "        <cbc:ID>1</cbc:ID>\n" +
                "        <cbc:InvoicedQuantity unitCode=\"PCE\">44.000000</cbc:InvoicedQuantity>\n" +
                "        <cbc:LineExtensionAmount currencyID=\"SAR\">966.00</cbc:LineExtensionAmount>\n" +
                "        <cac:TaxTotal>\n" +
                "            <cbc:TaxAmount currencyID=\"SAR\">144.90</cbc:TaxAmount>\n" +
                "            <cbc:RoundingAmount currencyID=\"SAR\">1110.90</cbc:RoundingAmount>\n" +
                "        </cac:TaxTotal>\n" +
                "        <cac:Item>\n" +
                "            <cbc:Name>dsd</cbc:Name>\n" +
                "            <cac:ClassifiedTaxCategory>\n" +
                "                <cbc:ID>S</cbc:ID>\n" +
                "                <cbc:Percent>15.00</cbc:Percent>\n" +
                "                <cac:TaxScheme>\n" +
                "                    <cbc:ID>VAT</cbc:ID>\n" +
                "                </cac:TaxScheme>\n" +
                "            </cac:ClassifiedTaxCategory>\n" +
                "        </cac:Item>\n" +
                "        <cac:Price>\n" +
                "            <cbc:PriceAmount currencyID=\"SAR\">22.00</cbc:PriceAmount>\n" +
                "            <cac:AllowanceCharge>\n" +
                "                <cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "                <cbc:AllowanceChargeReason>discount</cbc:AllowanceChargeReason>\n" +
                "                <cbc:Amount currencyID=\"SAR\">2.00</cbc:Amount>\n" +
                "            </cac:AllowanceCharge>\n" +
                "        </cac:Price>\n" +
                "    </cac:InvoiceLine>\n" +
                "</Invoice>");
        writeQr(fatootaQr, "image_by_fatoora.jpg");

    }

    private static void writeQr(String qr, String name) throws WriterException, IOException {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = writer.encode(qr, BarcodeFormat.QR_CODE, 512, 512);
        BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
        File outputfile = new File(name);
        ImageIO.write(bufferedImage, "jpg", outputfile);
    }
}
